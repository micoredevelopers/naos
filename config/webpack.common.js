const fs = require('fs')
const paths = require('./paths')

const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const pages = fs.readdirSync(paths.src + '/pages').filter(fileName => fileName.endsWith('.pug'))

module.exports = {
  // Where webpack looks to start building the bundle
  entry: [paths.src + '/js/app.js', paths.src + '/styles/style.scss'],
  
  // Where webpack outputs the assets and bundles
  output: {
    publicPath: '/',
    path: paths.build,
    filename: '[name].bundle.js'
  },
  
  // Customize the webpack build process
  plugins: [
    // Removes/cleans build folders and unused assets when rebuilding
    new CleanWebpackPlugin(),
    
    // Copies files from target to destination folder
    new CopyWebpackPlugin({
      patterns: [
        {
          from: paths.public,
          to: paths.build,
          globOptions: {
            ignore: ['*.DS_Store'],
          },
          noErrorOnMissing: true,
        }
      ]
    }),
    
    // Generates an HTML file from a .pug files
    ...pages.map(page => new HtmlWebpackPlugin({
      inject: 'body',
      template: `${paths.src + '/pages'}/${page}`,
      favicon: paths.public + '/favicon.svg',
      filename: `./${page.replace(/\.pug/, '.html')}`
    }))
  ],
  
  // Determine how modules within the project are treated
  module: {
    rules: [
      // Pug files: Build html files with pug-loader
      { test: /\.pug$/, exclude: /node_modules/, use: ['pug-loader'] },
      
      // JavaScript: Use Babel to transpile JavaScript files
      { test: /\.js$/, exclude: /node_modules/, use: ['babel-loader'] },
  
      { test: /\.(?:ico|gif|png|jpg|jpeg)$/i, type: 'asset/resource' },
  
      {
        test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 100,
            name: '[name].[ext]',
            outputPath: 'fonts/'
          }
        }
      }
    ]
  }
}
