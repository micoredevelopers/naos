import {hideContact, initMainAnimation, showContact, showMobileContact} from '../utils/animation'
import {initMainSlider} from '../utils/slider'

let lastScrollTop = 0

$(window).on('load', function () {
    initMainSlider()

    $(window).on('scroll', function() {
        let st = window.pageYOffset || document.documentElement.scrollTop

        if (st > lastScrollTop) {
            $('header').addClass('active')
        }

        if (st <= 0) {
            $('header').removeClass('active')
        }

        lastScrollTop = st <= 0 ? 0 : st
    })


    /* Animations */
    setTimeout(function () {
        initMainAnimation()
        // initLogoAnimation()
    }, 1500)

    /* Events */
    $('.contact-section__back-title').on('click', function () {
        if (window.innerWidth > 992 && $('.col-star').hasClass('end-move')) {
            hideContact()
        } else {
            $('html, body').animate({scrollTop: 0}, 500)
        }
    })

    $('.slider-button').on('click', function () {
        if (window.innerWidth < 992) {
            showMobileContact()
        } else {
            showContact()
        }
    })

    $('#contact-form').on('submit', function (e) {
        //обязательные поля: name, email, phone
        e.preventDefault()
        const $this = $(e.target)
        const data = $this.serialize();
        const url = '/back/public/feedback';

        $.ajax({
            url,
            data: data,
            method: 'POST',
            dataType: "json",
            crossDomain: true,
            success: function (response) {
                $this[0].reset();
                $.notify(response.message, {
                    type: 'success',
                    duration: 10,
                    placement: {
                        from: "bottom",
                        align: "right"
                    },
                });
            },
            error: function (res, status) {
                const validationErrorCode = 422
                if (res.status === validationErrorCode) {
                    $.each(res.responseJSON.errors, function ($key, $errorBag) {
                        $.each($errorBag, function ($key, $message) {
                            $.notify($message, {
                                type: 'danger',
                                duration: 10,
                                placement: {
                                    from: "bottom",
                                    align: "right"
                                },
                            });
                        })
                    });
                }
            }
        })
    })
})
