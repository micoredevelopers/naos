const $sliderBlock = $('.col-slider')
const $starBlock = $('.col-star, .star-mask')
const $contactBlock = $('.col-contact, .contact-section')

function getRandomIntBetween(min, max) {
  return Math.floor((Math.random() * (max - min) + min))
}

function clearMainAnimation() {
  $('[data-animation]').each((i, el) => {
    $(el).removeAttr('data-animation')
    $(el).removeAttr('data-delay')
    $(el).removeAttr('style')
  })
}

function pulse() {
  $('.star-mask, .star-pulse').addClass('pulse')

  setTimeout(function () {
    $('.star-mask, .star-pulse').removeClass('pulse')
  }, 4000)
}

function initMainAnimation() {
  $('[data-delay]').each((i, el) => {
    const delay = Number($(el).attr('data-delay'))

    $(el).css('animation-delay', `${Math.round(2300 + delay)}ms`)
  })

  setTimeout(function () {
    pulse()
  }, 2500)

  setTimeout(function () {
    initCometAnimation()

    setInterval(function () {
      initCometAnimation()
    }, 3000)
  }, 4500)

  setTimeout(function () {
    clearMainAnimation()
  }, 4000)
}

function initLogoAnimation() {
  const $flare = $('.header__flare')

  setInterval(function() {
    const $randFlare = $($flare.get(getRandomIntBetween(0, 4)))

    $flare.removeClass('show')
    $randFlare.addClass('show')
  }, 4000)
}

function showMobileContact() {
  $('#main-page').addClass('scroll-enable')
  $('html, body').animate({ scrollTop: $(`#contact`).offset().top - 100 }, 500)
}

function showContact() {
  $sliderBlock.addClass('hide')
  $starBlock.addClass('start-move')

  setTimeout(function() {
    $contactBlock.addClass('show')
    $contactBlock.removeClass('hide')
  }, 1500)

  setTimeout(function () {
    $starBlock.addClass('end-move')
    $starBlock.removeClass('start-move')
    $starBlock.removeClass('end-back-move')
  }, 2000)

  setTimeout(function () {
    $contactBlock.removeClass('show')
  }, 2500)
}

function hideContact() {
  $contactBlock.addClass('back-hide')

  setTimeout(function () {
    $contactBlock.addClass('hide')
    $starBlock.addClass('start-back-move')
  }, 1500)

  setTimeout(function () {
    $starBlock.removeClass('end-move')
    $starBlock.addClass('end-back-move')
    $contactBlock.removeClass('back-hide')
    $starBlock.removeClass('start-back-move')
  }, 3500)

  setTimeout(function () {
    $sliderBlock.addClass('show')
    $sliderBlock.removeClass('hide')
  }, 4000)
}

function initCometAnimation() {
  const $cometsWrap = $('.comets-wrap')
  const randArr = [...Array(getRandomIntBetween(15, 20))].map((v, i) => i)

  $cometsWrap.html('')

  randArr.forEach((v, i, arr) => {
    const position = getRandomIntBetween(10, 100)
    const delay = i * getRandomIntBetween(100, 200)

    $cometsWrap.append(`<span class="comet" style="left: calc(${position}% + 40px); animation-delay: ${delay}ms"></span>`)

    if (v === arr.length - 1) {
      $cometsWrap.addClass('start')
    }
  })
}

export { initMainAnimation, initLogoAnimation, showMobileContact, showContact, hideContact, pulse }
