const $slider = $('.main-slider')
const $sliderProgressBar = $('.slider-progress-bar')

export function initMainSlider() {
  $slider.on('init', function() {
    setTimeout(function () {
      $sliderProgressBar.addClass('start')

      let startSlick = () => {
        $slider.slick('slickNext');
      }

      let firstSlick = setInterval(startSlick, 10000)

      let initSlick


      $($('.slick-dots > li')).click(function () {
        let active = $(this).addClass('slick-active');
         let val = active.find('.slider-number').text();
         $sliderProgressBar.removeClass(`start01`);
         $sliderProgressBar.removeClass(`start02`);
         $sliderProgressBar.removeClass(`start03`);
         $sliderProgressBar.removeClass(`start`);
         $sliderProgressBar.addClass(`start${val}`);
         if($sliderProgressBar.hasClass('start03')) {
           setTimeout(function () {
             $sliderProgressBar.removeClass(`start03`);
             $sliderProgressBar.addClass(`start`);
           },10000)
         }else if ($sliderProgressBar.hasClass('start02')) {
           setTimeout(function () {
             $sliderProgressBar.removeClass(`start02`);
             $sliderProgressBar.addClass(`start`);
           },20000)
         } else if ($sliderProgressBar.hasClass('start01')) {
           setTimeout(function () {
             $sliderProgressBar.removeClass(`start01`);
             $sliderProgressBar.addClass(`start`);
           },30000)
         }
         clearInterval(firstSlick);
         clearInterval(initSlick);
         initSlick = setInterval(startSlick, 10000);



      })
    }, 4000)
  })



  $slider.slick({
    dots: true,
    fade: true,
    speed: 1000,
    arrows: false,
    slidesToShow: 1,
    draggable: false,
    slidesToScroll: 1,
    customPaging: (slick,index) => {
      const delay = 2300 + 700 + (100 * (index + 1))

      return `<p class="slider-number" style="animation-delay: ${delay}ms">0${index + 1}</p>`
    }
  })

  $slider.on('beforeChange', function() {
    $slider.find('.slide:not(.slick-cloned)').addClass('start-change')
    $slider.find('.slide:not(.slick-cloned)').removeClass('end-change')
  })

  $slider.on('afterChange', function() {
    $slider.find('.slide:not(.slick-cloned)').addClass('end-change')
    $slider.find('.slide:not(.slick-cloned)').removeClass('start-change')
  })
}
